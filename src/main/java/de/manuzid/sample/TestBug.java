package de.manuzid.sample;

public class TestBug {

    public static void main(String[] args) {
        if (args.length < 0)
            new IllegalArgumentException("x must be nonnegative");
    }

    int foo(int a) {
        int b = 12;
        if (a == 1) {
            return b;
        }
        return b;
    }

}
