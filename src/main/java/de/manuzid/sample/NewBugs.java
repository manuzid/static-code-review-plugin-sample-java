package de.manuzid.sample;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class NewBugs {

    private Thread mon1;
    private Thread mon2;

    public void tmp(String test) throws InterruptedException {
        synchronized (this.mon1) {
            synchronized (this.mon2) {
                this.mon2.wait();
            }
        }
    }

    private void tmp2() throws IOException {
        FileOutputStream fos = new FileOutputStream("fileName ", true);
        ObjectOutputStream out = new ObjectOutputStream(fos);
    }

    private boolean notReady() {
        return true;
    }

    private void tmp3() throws InterruptedException {
        synchronized ("monitor") {
            while (notReady()) {
                Thread.sleep(200);
            }
            notReady();
        }
    }

    private void tmp4() {
        for (;;) {
            // ...
        }

    }

    private void tmp5() {
        int j = 0;
        while (true) {
            j++;
        }

    }

}
